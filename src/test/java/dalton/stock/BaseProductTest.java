package dalton.stock;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BaseProductTest {
	private Product product;
	
	@Before
	public void init(){
		product  = new BaseProduct(10, "chips");
	}
	
	@Test
	public void canGetName(){
		assertEquals("chips", product.getName());
	}
	
	@Test
	public void canGetPrice(){
		assertEquals(10,product.getPrice());
	}
	
}
