package dalton.stock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

public class StackProductStockManagerTest {
	ProductStockManager stockManager;
	Map<Integer,Stack<Product>> products;
	Product product;

	@Before
	public void init() {
		Stack<Product> productSlot = new Stack<>();
		product = new BaseProduct(10, "chips");
		productSlot.push(product);

		products = new HashMap<>();
		products.put(0,productSlot);

		stockManager = new StackProductStockManager();
		ReflectionTestUtils.setField(stockManager, "products", products);
	}

	@Test
	public void canAddStock() {
		Product product = new BaseProduct(50, "chips");

		stockManager.add(0, product);

		assertEquals(product, products.get(0).peek());
	}

	@Test
	public void canPeek() {
		Product resultProduct = stockManager.peek(0);
		assertEquals(product, resultProduct);
	}

	@Test
	public void canPeekNoProductsInSlot() {
		Product resultProduct = stockManager.peek(1);
		assertNull(resultProduct);
	}

	@Test
	public void canPeekSlotDoesntExist() {
		Product resultProduct = stockManager.peek(100);
		assertNull(resultProduct);
	}

	@Test
	public void canRemove() {

		boolean result = stockManager.remove(0);

		assertTrue(result);
		assertTrue(products.get(0).isEmpty());
	}

	@Test
	public void canRemoveNoProductsInSlot() {
		boolean result = stockManager.remove(1);
		assertFalse(result);
	}

	@Test
	public void canRemoveSlotDoesntExist() {
		boolean result = stockManager.remove(100);
		assertFalse(result);
	}
}
