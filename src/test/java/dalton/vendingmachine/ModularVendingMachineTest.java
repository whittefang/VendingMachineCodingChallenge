package dalton.vendingmachine;

import static org.junit.Assert.assertEquals;

import org.easymock.EasyMock;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import dalton.change.ChangeBank;
import dalton.hardware.SimplifiedVendingMachineHardwareFunctions;
import dalton.stock.BaseProduct;
import dalton.stock.Product;
import dalton.stock.ProductStockManager;

public class ModularVendingMachineTest extends EasyMockSupport {
	private ModularVendingMachine vendingMachine;
	private ChangeBank changeBank;
	private SimplifiedVendingMachineHardwareFunctions hardwareFunctions;
	private ProductStockManager productStockManager;

	ReflectionTestUtils reflectionTestUtils;
	
	@Before
	public void init() {
		changeBank = createMock(ChangeBank.class);
		hardwareFunctions = createMock(SimplifiedVendingMachineHardwareFunctions.class);
		productStockManager = createMock(ProductStockManager.class);

		vendingMachine = new ModularVendingMachine(changeBank, hardwareFunctions, productStockManager);
	}

	@Test
	public void canAddUserMoney() {
		EasyMock.expect(changeBank.addChange(10)).andReturn(true);
		replayAll();
		vendingMachine.addUserMoney(10);
		verifyAll();
		assertEquals(10, ReflectionTestUtils.getField(vendingMachine, "currentMoney"));
	}

	@Test
	public void canAddUserMoneyMultipleCoins() {
		EasyMock.expect(changeBank.addChange(10)).andReturn(true);
		EasyMock.expect(changeBank.addChange(10)).andReturn(true);
		replayAll();
		vendingMachine.addUserMoney(10);
		vendingMachine.addUserMoney(10);
		verifyAll();
		assertEquals(20, ReflectionTestUtils.getField(vendingMachine, "currentMoney"));
	}

	@Test
	public void canAddUserMoneyNotAccepted() {
		EasyMock.expect(changeBank.addChange(2)).andReturn(false);
		hardwareFunctions.showMessage("currency not accepted");
		replayAll();
		vendingMachine.addUserMoney(2);
		verifyAll();
		assertEquals(0, ReflectionTestUtils.getField(vendingMachine, "currentMoney"));
	}

	@Test
	public void canAddProduct() {
		Product product = new BaseProduct(1, "chips");
		productStockManager.add(1, product);
		replayAll();
		vendingMachine.addProduct(1, product);
		verifyAll();
	}

	@Test
	public void canButtonPressrDisplayEmptyProduct() {
		EasyMock.expect(productStockManager.peek(1)).andReturn(null);
		hardwareFunctions.showMessage("slot empty");
		replayAll();
		vendingMachine.buttonPress(1);
		verifyAll();
	}

	@Test
	public void canButtonPressDisplayPrice() {
		Product product = new BaseProduct(10, "chips");
		EasyMock.expect(productStockManager.peek(1)).andReturn(product);
		hardwareFunctions.showMessage("10");
		replayAll();
		vendingMachine.buttonPress(1);
		verifyAll();
	}

	@Test
	public void canButtonPressVendProduct() {

		ReflectionTestUtils.setField(vendingMachine, "currentMoney", 15);
		Product product = new BaseProduct(10, "chips");
		EasyMock.expect(productStockManager.peek(1)).andReturn(product);
		EasyMock.expect(changeBank.vendChange(5)).andReturn(true);
		EasyMock.expect(productStockManager.remove(1)).andReturn(true);
		hardwareFunctions.dispenseProduct(1, "chips");
		
		replayAll();
		vendingMachine.buttonPress(1);
		verifyAll();
		
		assertEquals(0, ReflectionTestUtils.getField(vendingMachine, "currentMoney"));
	}

	@Test
	public void canButtonPressReturnChange() {
		ReflectionTestUtils.setField(vendingMachine, "currentMoney", 15);
		Product product = new BaseProduct(10, "chips");
		EasyMock.expect(productStockManager.peek(1)).andReturn(product);
		EasyMock.expect(changeBank.vendChange(5)).andReturn(false);
		EasyMock.expect(changeBank.vendChange(15)).andReturn(true);
		hardwareFunctions.showMessage("insufficient change left in machine, please insert exact change");
		
		replayAll();
		vendingMachine.buttonPress(1);
		verifyAll();
		
		assertEquals(0, ReflectionTestUtils.getField(vendingMachine, "currentMoney"));
	}
}
