package dalton.demo;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import dalton.change.ChangeBank;
import dalton.change.ConfigurableChangeBank;
import dalton.hardware.SimplifiedVendingMachineHardwareFunctions;
import dalton.hardware.SimplifiedVendingMachineHardwareFunctionsImpl;
import dalton.hardware.VendingMachineHardwareFunctions;
import dalton.stock.BaseProduct;
import dalton.stock.ProductStockManager;
import dalton.stock.StackProductStockManager;
import dalton.vendingmachine.ModularVendingMachine;
import dalton.vendingmachine.VendingMachine;

public class Demo {
	private VendingMachine vendingMachine;

	@Before
	public void init() {
		SortedMap<Integer, Integer> currency = new TreeMap<>();
		currency.put(25, 10);
		currency.put(10, 10);
		currency.put(5, 10);

		SimplifiedVendingMachineHardwareFunctions hardwareFunctions = new SimplifiedVendingMachineHardwareFunctionsImpl(
				new VendingMachineHardwareFunctions() {
				});
		ProductStockManager productStockManager = new StackProductStockManager();
		ChangeBank changeBank = new ConfigurableChangeBank(currency, hardwareFunctions);

		vendingMachine = new ModularVendingMachine(changeBank, hardwareFunctions, productStockManager);

		vendingMachine.addProduct(1, new BaseProduct(45, "candy"));
	}

	@Test
	public void canVend() {
		System.out.println("\n******Example of a user using most of the vending machines capabilities******");
		// add product
		vendingMachine.addProduct(0, new BaseProduct(85, "chips"));

		// display Price
		vendingMachine.buttonPress(0);

		// add money
		vendingMachine.addUserMoney(25);
		vendingMachine.addUserMoney(25);
		vendingMachine.addUserMoney(25);
		vendingMachine.addUserMoney(25);

		// vend product
		vendingMachine.buttonPress(0);

		// slot 0 is now empty
		vendingMachine.buttonPress(0);
	}

	@Test
	public void useCase1() {
		System.out.println("\n******Example of how a user can display price******");
		// display Price
		vendingMachine.buttonPress(1);
	}
	
	@Test
	public void useCase2() {
		System.out.println("\n******Example of how a user can add coins and then buy a product******");
		// add money
		vendingMachine.addUserMoney(25);
		vendingMachine.addUserMoney(25);
		
		// display Price
		vendingMachine.buttonPress(1);
	}
	
	@Test
	public void useCase3() {
		System.out.println("\n******Example of how a user can add a product to the machine******");
		// add product
		vendingMachine.addProduct(10, new BaseProduct(45, "chips"));

		// display Price
		vendingMachine.buttonPress(10);
	}
}
