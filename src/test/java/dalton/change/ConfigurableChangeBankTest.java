package dalton.change;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.SortedMap;
import java.util.TreeMap;

import org.easymock.EasyMock;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;

import dalton.hardware.SimplifiedVendingMachineHardwareFunctions;

public class ConfigurableChangeBankTest extends EasyMockSupport {
	ChangeBank changeBank;
	SimplifiedVendingMachineHardwareFunctions hardwareFunctions;
	SortedMap<Integer, Integer> currency;

	@Before
	public void init() {
		hardwareFunctions = createMock(SimplifiedVendingMachineHardwareFunctions.class);
		currency = new TreeMap<Integer, Integer>();
		currency.put(25, 1);
		currency.put(10, 1);
		currency.put(5, 1);
		changeBank = new ConfigurableChangeBank(currency, hardwareFunctions);
	}

	@Test
	public void canAddChange() {
		boolean result = changeBank.addChange(25);
		assertTrue(result);
		assertEquals(new Integer(2), currency.get(25));
	}

	@Test
	public void canAddChangeNegative() {
		boolean result = changeBank.addChange(50);
		assertFalse(result);
	}

	@Test
	public void canVendChangeSingleCoin() {
		EasyMock.expect(hardwareFunctions.dispenseChange(25)).andReturn(true);
		
		replayAll();
		boolean result = changeBank.vendChange(25);
		verifyAll();
		
		assertTrue(result);
		assertEquals(new Integer(0), currency.get(25));
		assertEquals(new Integer(1), currency.get(10));
		assertEquals(new Integer(1), currency.get(5));
	}

	@Test
	public void canVendChangeMultipleCoins() {
		EasyMock.expect(hardwareFunctions.dispenseChange(25)).andReturn(true);
		EasyMock.expect(hardwareFunctions.dispenseChange(10)).andReturn(true);
		EasyMock.expect(hardwareFunctions.dispenseChange(5)).andReturn(true);
		
		replayAll();
		boolean result = changeBank.vendChange(40);
		verifyAll();
		
		assertTrue(result);
		assertEquals(new Integer(0), currency.get(25));
		assertEquals(new Integer(0), currency.get(10));
		assertEquals(new Integer(0), currency.get(5));
	}

	@Test
	public void canVendChangeNoChangeRequired() {
		
		replayAll();
		boolean result = changeBank.vendChange(0);
		verifyAll();
		
		assertTrue(result);
		assertEquals(new Integer(1), currency.get(25));
		assertEquals(new Integer(1), currency.get(10));
		assertEquals(new Integer(1), currency.get(5));
	}
	@Test
	public void canVendChangeInsufficientChange() {
		boolean result = changeBank.vendChange(45);
		assertFalse(result);
		assertEquals(new Integer(1), currency.get(25));
		assertEquals(new Integer(1), currency.get(10));
		assertEquals(new Integer(1), currency.get(5));

	}
	
	@Test
	public void canVendChangeLargeAmount() {
		currency.put(25, 2);
		currency.put(10, 5);
		currency.put(5, 2);
		

		EasyMock.expect(hardwareFunctions.dispenseChange(25)).andReturn(true).times(2);
		EasyMock.expect(hardwareFunctions.dispenseChange(10)).andReturn(true).times(5);
		EasyMock.expect(hardwareFunctions.dispenseChange(5)).andReturn(true).times(2);

		replayAll();
		boolean result = changeBank.vendChange(110);
		verifyAll();
		assertTrue(result);
		assertEquals(new Integer(0), currency.get(25));
		assertEquals(new Integer(0), currency.get(10));
		assertEquals(new Integer(0), currency.get(5));
		

	}

}
