package dalton.hardware;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;

public class SimplifiedVendingMachineHardwareFunctionsImplTest extends EasyMockSupport {
	private SimplifiedVendingMachineHardwareFunctionsImpl simplifiedHardwareFunctions;
	private VendingMachineHardwareFunctions hardwareFunctions;

	@Before
	public void init() {
		hardwareFunctions = createMock(VendingMachineHardwareFunctions.class);
		simplifiedHardwareFunctions = new SimplifiedVendingMachineHardwareFunctionsImpl(hardwareFunctions);
	}

	@Test
	public void canDispenseChangeQuarter() {
		hardwareFunctions.dispenseQuarter();

		replayAll();
		boolean result = simplifiedHardwareFunctions.dispenseChange(25);
		verifyAll();
		assertTrue(result);
	}

	@Test
	public void canDispenseChangeDime() {
		hardwareFunctions.dispenseDime();

		replayAll();
		boolean result = simplifiedHardwareFunctions.dispenseChange(10);
		verifyAll();
		assertTrue(result);
	}

	@Test
	public void canDispenseChangeNickel() {
		hardwareFunctions.dispenseNickel();

		replayAll();
		boolean result = simplifiedHardwareFunctions.dispenseChange(5);
		verifyAll();
		assertTrue(result);
	}

	@Test
	public void canDispenseChangeNegative() {
		replayAll();
		boolean result = simplifiedHardwareFunctions.dispenseChange(2);
		verifyAll();
		assertFalse(result);
	}

	@Test
	public void canShowMessage() {
		hardwareFunctions.showMessage("test");
		replayAll();
		simplifiedHardwareFunctions.showMessage("test");
		verifyAll();
	}

	@Test
	public void canDispenseProduct() {
		hardwareFunctions.dispenseProduct(1, "product");
		replayAll();
		simplifiedHardwareFunctions.dispenseProduct(1, "product");
		verifyAll();
	}
}
