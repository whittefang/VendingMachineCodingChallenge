package dalton.stock;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class StackProductStockManager implements ProductStockManager {
	private Map<Integer,Stack<Product>> products;

	public StackProductStockManager() {
		products = new HashMap<>();

	}

	@Override
	public Product peek(Integer position) {
		try {
			Stack<Product> productSlot = products.get(position);
			Product requestedProduct = productSlot.peek();
			return requestedProduct;
		} catch (NullPointerException | EmptyStackException e) {
			return null;
		}
	}

	@Override
	public boolean remove(Integer position) {
		try {
			Stack<Product> productSlot = products.get(position);
			productSlot.pop();
			return true;
		} catch (NullPointerException | EmptyStackException e) {
			return false;
		}
	}

	@Override
	public void add(Integer slot, Product product) {
		// check if we have a stack in selected slot, if now then add it
		if (!products.containsKey(slot)){
			 products.put(slot, new Stack<Product>());
		}
		Stack<Product> productSlot = products.get(slot);
		productSlot.push(product);
	}
	
}
