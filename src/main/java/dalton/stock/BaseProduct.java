package dalton.stock;

public class BaseProduct implements Product{
	private Integer price;
	private String name;
	
	public BaseProduct(Integer price, String name) {
		this.price = price;
		this.name = name;
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public String getName() {
		return name;
	}

}
