package dalton.stock;

public interface ProductStockManager {
	Product peek(Integer position);
	// returns true if product was able to be removed, otherwise false
	boolean remove(Integer position);
	void add(Integer slot, Product product);
}
