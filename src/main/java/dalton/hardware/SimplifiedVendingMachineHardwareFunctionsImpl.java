package dalton.hardware;

public class SimplifiedVendingMachineHardwareFunctionsImpl implements SimplifiedVendingMachineHardwareFunctions {
	VendingMachineHardwareFunctions hardwareFunctions;

	public SimplifiedVendingMachineHardwareFunctionsImpl(VendingMachineHardwareFunctions hardwareFunctions) {
		this.hardwareFunctions = hardwareFunctions;
	}

	@Override
	public boolean dispenseChange(Integer amount) {
		boolean success = false;

		if (amount == 25) {
			hardwareFunctions.dispenseQuarter();
			success = true;
		} else if (amount == 10) {
			hardwareFunctions.dispenseDime();
			success = true;
		} else if (amount == 5) {
			hardwareFunctions.dispenseNickel();
			success = true;
		}
		return success;
	}

	@Override
	public void showMessage(String message) {
		hardwareFunctions.showMessage(message);

	}

	@Override
	public void dispenseProduct(Integer productPosition, String productName) {
		hardwareFunctions.dispenseProduct(productPosition, productName);
	}

}
