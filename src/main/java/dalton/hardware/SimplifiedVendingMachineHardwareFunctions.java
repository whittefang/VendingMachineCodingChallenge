package dalton.hardware;

public interface SimplifiedVendingMachineHardwareFunctions {
	void showMessage(String message);

    void dispenseProduct(Integer productPosition, String productName);

    boolean dispenseChange(Integer amount);

}
