package dalton.hardware;

public interface VendingMachineHardwareFunctions {
	default void showMessage(String message) {
        System.out.println(message);
    }

    default void dispenseProduct(Integer productPosition, String productName) {
        String nullSafeProductName = (productName != null) ? productName : "ProductNum" + productPosition;
        System.out.println("Dispensing " + nullSafeProductName + " from position " + productPosition);
    }

    default void dispenseNickel() {
        System.out.println("Dispensing 5 cents");
    }

    default void dispenseDime() {
        System.out.println("Dispensing 10 cents");
    }

    default void dispenseQuarter() {
        System.out.println("Dispensing 25 cents");
    }
}
