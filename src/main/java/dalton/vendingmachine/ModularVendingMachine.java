package dalton.vendingmachine;

import dalton.change.ChangeBank;
import dalton.hardware.SimplifiedVendingMachineHardwareFunctions;
import dalton.stock.Product;
import dalton.stock.ProductStockManager;

public class ModularVendingMachine implements VendingMachine {

	private int currentMoney;
	private ChangeBank changeBank;
	private SimplifiedVendingMachineHardwareFunctions hardwareFunctions;
	private ProductStockManager productStockManager;


	public ModularVendingMachine(ChangeBank changeBank, SimplifiedVendingMachineHardwareFunctions hardwareFunctions,ProductStockManager productStockManager) {
		this.changeBank = changeBank;
		this.hardwareFunctions = hardwareFunctions;
		this.productStockManager = productStockManager;
		currentMoney =0;
	}

	@Override
	public void buttonPress(Integer productPosition) {
		// if there is a product in selected slot get it
		Product requestedProduct = productStockManager.peek(productPosition);
		if (requestedProduct == null) {
			hardwareFunctions.showMessage("slot empty");
			return;
		}

		// if enough money has been inserted vend the item, otherwise display price
		if (currentMoney >= requestedProduct.getPrice()) {
			boolean successfullyVendedChange = changeBank.vendChange(currentMoney - requestedProduct.getPrice());
			if (successfullyVendedChange){
				productStockManager.remove(productPosition);
				hardwareFunctions.dispenseProduct(productPosition, requestedProduct.getName());
			}else {
				hardwareFunctions.showMessage("insufficient change left in machine, please insert exact change");
				changeBank.vendChange(currentMoney);
			}
			currentMoney = 0;
		} else {
			hardwareFunctions.showMessage(String.valueOf(requestedProduct.getPrice()));
		}
	}

	@Override
	public void addUserMoney(Integer cents) {
		if (changeBank.addChange(cents)) {
			currentMoney += cents;
		} else {
			hardwareFunctions.showMessage("currency not accepted");
		}
	}

	@Override
	public void addProduct(Integer productPosition, Product productToAdd) {
		productStockManager.add(productPosition, productToAdd);
	}
	
}
