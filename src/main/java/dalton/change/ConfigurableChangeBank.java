package dalton.change;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import dalton.hardware.SimplifiedVendingMachineHardwareFunctions;

public class ConfigurableChangeBank implements ChangeBank {
	// currency contains types and amounts of currency we hold, key='currency
	// value' value='amount of currency we currently have'
	Map<Integer, Integer> currency;
	SimplifiedVendingMachineHardwareFunctions hardwareFunctions;

	public ConfigurableChangeBank(Map<Integer, Integer> currency,
			SimplifiedVendingMachineHardwareFunctions hardwareFunctions) {
		this.currency = currency;
		this.hardwareFunctions = hardwareFunctions;

	}

	@Override
	// returns true if change is added, false if it isn't
	public boolean addChange(Integer amount) {
		// if we support this currency type add it to the bank
		if (currency.containsKey(amount)) {
			currency.put(amount, currency.get(amount) + 1);
			return true;
		}
		return false;
	}

	@Override
	// returns true if change can be dispensed, false if there is insufficient change
	public boolean vendChange(Integer amount) {

		List<Integer> changeToBeDispensed = new ArrayList<>();
		// store current amounts of currency in a map sorted in descending order
		SortedMap<Integer, Integer> updatedCurrencyAmounts = new TreeMap<Integer, Integer>(Collections.reverseOrder());
		updatedCurrencyAmounts.putAll(currency);

		// determine if we have enough change to complete transaction
		while (amount > 0) {
			Integer startingAmount = amount;
			//  find the highest value coin that is less than or equal to remaining amount
			for (Integer currencyValue : updatedCurrencyAmounts.keySet()) {
				Integer amountOfCurrency = updatedCurrencyAmounts.get(currencyValue);
				if (amountOfCurrency > 0 && amount >= currencyValue) {
					amount -= currencyValue;
					updatedCurrencyAmounts.put(currencyValue, amountOfCurrency - 1);
					// store coin that we want to dispense
					changeToBeDispensed.add(currencyValue);
					break;
				}
			}

			if (startingAmount == amount) {
				// couldn't find sufficient change
				return false;
			}
		}

		// we have sufficient change, dispense it and save new currency amounts
		for (Integer changeAmount : changeToBeDispensed) {
			hardwareFunctions.dispenseChange(changeAmount);
		}
		currency.putAll(updatedCurrencyAmounts);
		return true;

	}
}
