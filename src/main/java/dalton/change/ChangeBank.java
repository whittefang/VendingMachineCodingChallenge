package dalton.change;

public interface ChangeBank {
	// returns true if change is able to be successfully added, false if change could not be added
	public boolean addChange(Integer amount);
	// returns true if change was able to be dispensed, false if unsuccessful
	public boolean vendChange(Integer amount);
}
